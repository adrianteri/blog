---
title: Foo Bar
date: 2020-05-27
tags: the_first_post
---

This is an example article. This blog has been generated using [middleman](https://middlemanapp.com/basics/blogging/).

![Adrian Teri](/blog/images/photo_2018-12-03_14-24-09.jpg)

The blog post itself is written in [markdown](https://www.markdownguide.org). 

You probably want to delete it and write your own articles!
